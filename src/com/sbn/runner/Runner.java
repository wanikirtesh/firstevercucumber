package com.sbn.runner;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/*******************************
 * Created by Kirtesh Wani on 15-07-2015.
 * for FirstEverCucumber
 *******************************/
@RunWith(Cucumber.class)
@CucumberOptions(features = "src/com/sbn/features",glue = "com.sbn.gluecode")
public class Runner {
}

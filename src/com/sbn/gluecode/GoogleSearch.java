package com.sbn.gluecode;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/*******************************
 * Created by Kirtesh Wani on 15-07-2015.
 * for FirstEverCucumber
 *******************************/
public class GoogleSearch {
    static WebDriver driver = new FirefoxDriver();
    @Given("^User has opened browser with url \"([^\"]*)\"$")
    public void User_has_opened_browser_with_url(String strUrl) throws Throwable {
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(strUrl);

    }

    @And("^Entered the search term \"([^\"]*)\"$")
    public void Entered_the_search_term(String strSearch) throws Throwable {
        driver.findElement(By.name("q")).sendKeys(strSearch);
    }

    @Then("^Below list should get as suggestion$")
    public void Below_list_should_get_as_suggestion(DataTable expectedData) throws Throwable {
        List<WebElement> suggestions = driver.findElements(By.xpath("//ul[@class='sbsb_b']//li"));
        List<String> expectedList = expectedData.asList(String.class);
        List<String> actualList = new ArrayList<String>();
        for(WebElement suggestion : suggestions) {
            actualList.add(suggestion.getText());
        }
        Object[] expectedArray = expectedList.toArray();
        Object[] actualArray = actualList.toArray();
        Arrays.sort(expectedArray);
        Arrays.sort(actualArray);
        Assert.assertArrayEquals(expectedArray, actualArray);

    }
}

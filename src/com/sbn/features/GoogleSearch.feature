Feature: To test the google search page

  Scenario: When use type in to search box relevant suggestions must be get in list.
    Given User has opened browser with url "http://www.google.com"
    And Entered the search term "Kirtesh"
    Then Below list should get as suggestion
      | kirtesh patel         |
      | kirtesh meaning       |
      | kirtesh jain          |
      | kirtesh kumar trivedi |